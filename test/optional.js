var expect = require('expect.js'),
    sinon = require('sinon'),
    Optional = require('../src/optional.js');

describe('Optional.empty', function() {
    var opt = Optional.empty();
    it('throws an exception if invoking get', function(done) {
        try {
            opt.get();
            done(new Error('Should throw an exception'));
        } catch (err) {
            done();
        }
    });

    it('returns true when invoking isEmpty', function(done) {
        expect(opt.isEmpty()).to.be(true);
        done();
    });

    it('returns false when invoking isPresent', function(done) {
        expect(opt.isPresent()).to.be(false);
        done();
    });

    it('does not invoke ifPresent consumer', function(done) {
        var consumer = function(value) {
            done(new Error('Should not be invoked'));
        };
        opt.ifPresent(consumer);
        done();
    });

    it('stream does return immediately', function(done) {
        var total = 0;
        for (const value of opt.stream()) {
            total++;
        }
        expect(total).to.be(0);
        done();
    });

    describe('ifPresentOrElse', function() {
        it('throws an exception when emptyFn is not a function', function(done) {
            try {
                opt.ifPresentOrElse(function() {}, 'notAFunction').
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });

        it('invokes the emptyFn when no value is present', function(done) {
            var argument = "notSet";
            var consumer = function() {
                argument = "set";
            };
            opt.ifPresentOrElse(function() {}, consumer);
            expect(argument).to.be("set");
            done();
        });
    });

    it('does not invoke the filter function', function(done) {
        var filter = function(value) {
            done(new Error('Should not be invoked'));
        };
        opt.filter(filter);
        done();
    });

    it('does return an empty optional when invoking map', function(done) {
        var mapper = function(value) {
            done(new Error('Should not be invoked'));
        };
        try {
            expect(opt.map(mapper).get());
            done(new Error('Should not be reach this point'));
        } catch (err) {
            done();
        }
    });

    it('does return an empty optional when invoking flatmap', function(done) {
        var mapper = function(value) {
            done(new Error('Should not be invoked'));
        };
        try {
            expect(opt.flatMap(mapper).get());
            done(new Error('Should not be reach this point'));
        } catch (err) {
            done();
        }
    });

    it('does return the other when orElse', function(done) {
        var result = opt.orElse('goodbye');
        expect(result).to.be('goodbye');
        done();
    });

    describe('or', function() {
        it('does throw an error if function is null', function(done) {
            try {
                expect(opt.or(null));
                done(new Error('Should not be reach this point'));
            } catch (err) {
                done();
            }
        });

        it('does return the value from the function', function(done) {
            var fn = function() {
                return "hello";
            };

            var result = opt.or(fn);
            expect(result.get()).to.be('hello');
            done();
        });
    });

    describe('orElseGet', function() {
        it('does invoke the supplier', function(done) {
            var throwError = true;
            var supplier = function() {
                done();
                throwError = false;
            };
            opt.orElseGet(supplier);
            if (throwError) {
                done(new Error('Should invoke the supplier instead'));
            }
        });

        it('does throw an exception if the supplier is null', function(done) {
            try {
                opt.orElseGet(null);
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });

        it('does throw an exception if the supplier is not a function', function(done) {
            try {
                opt.orElseGet("null");
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });
    });

    describe('orElseThrow', function() {
        it('invokes the exception supplier', function(done) {
            var supplier = function() {
                return new Error('hello');
            };

            try {
                opt.orElseThrow(supplier);
                done(new Error('Should have thrown an exception'));
            } catch (err) {
                done();
            }
        });

        it('does throw an exception if the supplier is null', function(done) {
            try {
                opt.orElseThrow(null);
            } catch (err) {
                console.log(err.message);
                done();
            }
        });

        it('does throw an exception if the supplier is not a function', function(done) {
            try {
                opt.orElseThrow("null");
            } catch (err) {
                console.log(err.message);
                done();
            }
        });
    });
});

describe('Optional.of', function() {
    var opt = Optional.of("hello");
    describe('creation', function() {
        it('can invoke get successfully', function(done) {
            expect(opt.get()).to.be("hello");
            done();
        });

        it('throws an exception when creating with a null value', function(done) {
            try {
                Optional.of(null);
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });
    });

    describe('isPresent', function() {
        it('returns true when invoking isPresent', function(done) {
            expect(opt.isPresent()).to.be(true);
            done();
        });
    });

    describe('isEmpty', function() {
        it('returns false when invoking isEmpty', function(done) {
            expect(opt.isEmpty()).to.be(false);
            done();
        });
    });

    describe('ifPresent', function() {
        it('throws an exception when consumer is not a function', function(done) {
            try {
                opt.ifPresent('notAFunction');
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });

        it('invokes the consumer and keeps the value', function(done) {
            var input = "hello",
                argument;
            var consumer = function(input) {
                argument = input;
            };
            var result = opt.ifPresent(consumer);
            expect(argument).to.be(input);
            done();
        });
    });

    describe('ifPresentOrElse', function() {
        it('throws an exception when nonEmptyFn is not a function', function(done) {
            try {
                opt.ifPresentOrElse('notAFunction', function() {}).
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });

        it('invokes the nonEmptyFn if value present and keeps the value', function(done) {
            var input = "hello",
                argument;
            var consumer = function(input) {
                argument = input;
            };
            opt.ifPresentOrElse(consumer, function() {});
            expect(argument).to.be(input);
            done();
        });
    });

    describe('filter', function() {
        it('throws an exception if predicate it not a function', function(done) {
            try {
                opt.filter('notAFunction').
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });

        it('invokes the filter that evals to true', function(done) {
            var filter = function(input) {
                return true;
            };
            var result = opt.filter(filter);
            expect(result.get()).to.be("hello");
            done();
        });

        it('invokes the filter that evals to false', function(done) {
            var filter = function(input) {
                return false;
            };
            var result = opt.filter(filter);
            try {
                result.get();
                done(new Error('Evaluating empty one'));
            } catch (err) {
                done();
            }
        });
    });

    describe('map', function() {
        it('throws an exception if mapper it not a function', function(done) {
            try {
                opt.map('notAFunction').
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });

        it('creates an empty optional when the mapper returns null', function(done) {
            var mapper = function(input) {
                return null;
            };
            var result = opt.map(mapper);
            try {
                result.get();
                done(new Error('Evaluating empty optional'));
            } catch (err) {
                done();
            }
        });

        it('creates an optional when the mapper returns not null', function(done) {
            var mapper = function(input) {
                return "goodbye";
            };
            var result = opt.map(mapper);
            expect(result.get()).to.be("goodbye");
            done();
        });
    });

    describe('flatMap', function() {
        it('throws an exception if mapper it not a function', function(done) {
            try {
                opt.flatMap('notAFunction').
                done(new Error('Should throw an exception'));
            } catch (err) {
                done();
            }
        });

        it('throws an exception if the mapper returns null', function(done) {
            var mapper = function(input) {
                return null;
            };
            try {
                opt.flatMap(mapper);
                done(new Error('Should throw an exception when returning null'));
            } catch (err) {
                done();
            }
        });
    });

    describe('or', function() {
        it('returns the inner value', function(done) {
            expect(opt.or(function() {}).get()).to.be('hello');
            done();
        });
    });

    describe('orElses', function() {
        it('returns the defined value when invoking orElse', function(done) {
            expect(opt.orElse()).to.be('hello');
            done();
        });

        it('returns the defined value when invoking orElseGet', function(done) {
            expect(opt.orElseGet()).to.be('hello');
            done();
        });

        it('returns the defined value when invoking orElseThrow', function(done) {
            expect(opt.orElse()).to.be('hello');
            done();
        });
    });

    it('stream does not return immediately', function(done) {
        var total = 0;
        var result = 'notset';

        for (let value of opt.stream()) {
            total++;
            result = value;
        }
        expect(total).to.be(1);
        expect(result).to.be('hello');
        done();
    });

    it('is able to iterate over same optional multiple times', function(done) {
        var total = 0;

        for (let value of opt.stream()) {
            total++;
        }
        for (let value of opt.stream()) {
            total++;
        }
        expect(total).to.be(2);
        done();
    });

    it('stream over iterator', function(done) {
        var total = 0;
        var result = 'notset';

        for (let value of opt) {
            total++;
            result = value;
        }
        expect(total).to.be(1);
        expect(result).to.be('hello');
        done();
    });
});

describe('Optional.ofNullable', function() {
    it('does not throw an exception on creation with null value', function(done) {
        var opt = Optional.ofNullable(null);
        expect(opt.isPresent()).to.be(false);
        done();
    });

    it('does not throw an exception on creation with non null value', function(done) {
        var opt = Optional.ofNullable("hello");
        expect(opt.isPresent()).to.be(true);
        expect(opt.get()).to.be("hello");
        done();
    });
});
