/*jslint node: true */
'use strict';

module.exports = {
    /** Returns an empty Optional instance. No value is present for this Optional. */
    empty: () => {
        return new Optional();
    },

    /** Returns an Optional with the specified present non-null value. */
    of: (value) => {
        if (isNullOrUndefined(value)) {
            throw new Error('NullPointerException : value is not defined');
        }
        return new Optional(value);
    },

    /** Returns an Optional describing the specified value, if non-null, otherwise returns an empty Optional.*/
    ofNullable: (value) => {
        return new Optional(value);
    }
};

function isNullOrUndefined(value) {
    return (value === undefined || value === null);
};

function isFunction(value) {
    return typeof value === 'function';
};

function assertIsAFunction(fn, errorMessage) {
    if (isNullOrUndefined(fn) || !isFunction(fn))
        throw new Error(errorMessage);
};

var Optional = function(value) {
    /** If a value is present in this Optional, returns the value, otherwise throws Error. */
    this.get = () => {
        if (this.isEmpty()) {
            throw new Error('Can not invoke get on Optional with null/undefined value');
        }
        return value;
    };
    
    /**  If a value is not present, returns true, otherwise false. */
    this.isEmpty = () => isNullOrUndefined(value);

    /** Returns true if there is a value present, otherwise false. */
    this.isPresent = () => !this.isEmpty();

    /**
     * If a value is present, invoke the specified fn with the value, otherwise do nothing.
     *
     * @param {*} fn with the value as an argument
     */
    this.ifPresent = (fn) => {
        if (this.isPresent()) {
            assertIsAFunction(fn, 'ifPresent: argument is not a function');
            fn(value);
        }
    };

    /**
     * If a value is present, performs the given action with the value, otherwise performs the given empty-based action.
     *
     * @param {*} nonEmptyFn the function to be invoked, if a value is present with that value as argument.
     * @param {*} emptyFn the empty-based action to be invoked, if no value is present.
     */
    this.ifPresentOrElse = (nonEmptyFn, emptyFn) => {
        if (this.isPresent()) {
            assertIsAFunction(nonEmptyFn, 'ifPresentOrElse: nonEmptyFn is not a function');
            nonEmptyFn(value);
        } else {
            assertIsAFunction(emptyFn, 'ifPresentOrElse: emptyFn is not a function');
            emptyFn();
        }
    };

    /**
     * If a value is present, and the value matches the given predicate,
     * return an Optional describing the value, otherwise return an empty Optional.
     *
     * @param {*} fn filter function that takes the value as an argument.
     */
    this.filter = (fn) => {
        if (this.isEmpty()) {
            return this;
        } else {
            assertIsAFunction(fn, 'filter: argument is not a function');
            return (fn(value)) ? new Optional(value) : new Optional();
        }
    };

    /**
     * If a value is present, apply the provided mapping function to it,
     * and if the result is non-null, return an Optional describing the result.
     *
     * @param {*} fn with the value as an argument
     */
    this.map = (fn) => {
        if (this.isEmpty()) {
            return this;
        } else {
            assertIsAFunction(fn, 'map: argument is not a function');
            return new Optional(fn(value));
        }
    };

    /**
     * If a value is present, apply the provided Optional-bearing mapping function to it,
     * return that result, otherwise return an empty Optional.
     *
     * @param {*} fn mapping function that takes the value as argument
     */
    this.flatMap = (fn) => {
        if (this.isEmpty()) {
            return this;
        } else {
            assertIsAFunction(fn, 'flatMap: argument is not a function');
            let flatMappedValue = fn(value);

            if (isNullOrUndefined(flatMappedValue)) {
                throw new Error('Mapper must not return a null or undefined value');
            }

            return flatMappedValue;
        }
    };

    /** Return the value if present, otherwise return other. */
    this.orElse = (other) => this.isPresent() ? value : other;

    /** Return the value if present, otherwise invoke fn  and return the result of that invocation. */
    this.orElseGet = (fn) => {
        if (this.isPresent()) {
            return value;
        } else {
            assertIsAFunction(fn, 'orElseGet: argument is not a function');
            return fn();
        }
    };

    /**
     * If a value is present, returns an Optional describing the value, otherwise returns an Optional produced
     * by the supplying function.
     *
     * @param {*} fn the supplying function that produces an Optional to be returned
     *
     * @returns an Optional describing the value of this Optional, if a value is present,
     *          otherwise an Optional produced by the supplying function.
     */
    this.or = (fn) => {
        if (this.isPresent()) {
            return this;
        } else {
            assertIsAFunction(fn, 'or: argument is not a function');
            return new Optional(fn());
        }
    };

    /** Return the contained value, if present, otherwise throw an exception to be created by the provided fn. */
    this.orElseThrow = (fn) => {
        if (this.isPresent()) {
            return value;
        } else {
            assertIsAFunction(fn, 'orElseThrow: argument is not a function');
            throw fn();
        }
    };

    /** If a value is present, returns an iterable containing only that value, otherwise returns an empty iterable. */
    this.stream = function*() {
        if (this.isPresent()) {
            yield value;
        }
        return;
    };

    /** Fancy javascript iterable. Same contract as #stream method. */
    this[Symbol.iterator] = this.stream;
};
