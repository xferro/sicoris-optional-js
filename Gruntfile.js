module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    mochacli: {
      all: ['test/**/*.js'],
      options: {
        reporter: 'spec',
        ui: 'tdd'
      }
    },
    watch: {
      js: {
        files: ['src/**/*.js', '!node_modules/**/*.js'],
        tasks: ['default'],
        options: {
          nospawn: true
        }
      }
    },
    babel: {
      options: {
        comments: false,
        sourceMap: true,
        presets: ['@babel/preset-env']
      },
      dist: {
        files: {
          'dist/optional.js': 'src/optional.js'
        }
      }
    }
  });

  require("load-grunt-tasks")(grunt);
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-mocha-cli');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('test', ['mochacli', 'watch']);
  grunt.registerTask('ci', ['mochacli']);
  grunt.registerTask('default', ['babel', 'test']);
};
